#include "levelUpEnemy.h"
#include "Scene.h" 
#include "util.h"


levelUpEnemy::levelUpEnemy()
{
}

levelUpEnemy::~levelUpEnemy()
{
}

void levelUpEnemy::start()
{
	texture = loadTexture("gfx/player.png");
	directionX = rand() % -1 + 1;
	directionY = -1;
	width = 0;
	height = 0;
	speed = 1.5;
	health = 50;
	reloadtime = 60;//8 frames * 60sec  = 0.13 sec
	currentReloadTime = 0;
	directionChangetime = (rand() % 300) + 180; // 3 to 8 sec
	currentDirectionChangeTime = 1;

	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
	sound = SoundManager::loadSound("sound/196914__dpoggioli__laser-gun.ogg");
	sound->volume = 64;
}

void levelUpEnemy::update()
{
	
	x += directionX * speed;
	y += directionY * speed;
	/*texture = loadTexture(getTexture());*/

	if (currentDirectionChangeTime > 0)
		currentDirectionChangeTime--;

	if (currentDirectionChangeTime == 0)
	{
		directionY = -directionY;
		//directionX = -directionX;
		currentDirectionChangeTime = directionChangetime;
	}
	if (currentReloadTime > 0)
		currentReloadTime--;

	if (currentReloadTime == 0)
	{
		float dx = 0;
		float dy = -1;
		calcSlope(playerTarget->getPositionX(), playerTarget->getPositionY(), x, y, &dx, &dy);
		SoundManager::playSound(sound);
		Bullet* bullet1 = new Bullet(x - 20 + width, y - 2 + height / 2, dx, dy, 10, Side::ENEMY_SIDE);
		Bullet* bullet2 = new Bullet(x - 40 + width, y - 2 + height / 2, dx, dy, 10, Side::ENEMY_SIDE);
		Bullet* bullet3 = new Bullet(x - 60+ width, y - 2 + height / 2, dx, dy, 10, Side::ENEMY_SIDE);
		Bullet* bullet4 = new Bullet(x - 80+ width, y - 2 + height / 2, dx, dy, 10, Side::ENEMY_SIDE);
		Bullet* bullet5 = new Bullet(x - 100+ width, y - 2 + height / 2, dx, dy, 10, Side::ENEMY_SIDE);
		bullets.push_back(bullet1);
		bullets.push_back(bullet2);
		bullets.push_back(bullet3);
		bullets.push_back(bullet4);
		bullets.push_back(bullet5);
		getScene()->addGameObject(bullet1);
		getScene()->addGameObject(bullet2);
		getScene()->addGameObject(bullet3);
		getScene()->addGameObject(bullet4);
		getScene()->addGameObject(bullet5);

		//reset our load timmer
		currentReloadTime = reloadtime;
		for (int i = 0; i < bullets.size(); i++)
		{
			if (bullets[i]->getPositionX() < 0)
			{
				Bullet* bulletToErase = bullets[i];
				bullets.erase(bullets.begin() + i);
				delete bulletToErase;
				break;
			}
		}

	}
}

void levelUpEnemy::setPlayerTarget(Player* player)
{
	playerTarget = player;
}
void levelUpEnemy::draw()
{
	blitRotate(texture, x, y, 180.0f);
}
//void levelUpEnemy::setPlayerTarget(Player* player)
//{
//	playerTarget = player;
//}
void levelUpEnemy::setPosition(int xPos, int yPos)
{
	this->x = xPos;
	this->y = yPos;
}

void levelUpEnemy::setHealth(int variableHealth)
{
	health = variableHealth;
}

//void Enemy::setTexture(const char* filename)
//{
//	texture = loadTexture(filename);
//}

int levelUpEnemy::getPositionX()
{
	return x;
}

int levelUpEnemy::getPositionY()
{
	return y;
}

int levelUpEnemy::getWidth()
{
	return width;
}

int levelUpEnemy::getHeight()
{
	return height;
}

int levelUpEnemy::getHealth()
{
	return health;
}

