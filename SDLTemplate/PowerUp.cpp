#include "PowerUp.h"
#include "Scene.h" 
#include "util.h"
PowerUp::PowerUp()
{

}
PowerUp::~PowerUp()
{
}
void PowerUp::start()
{
	texture = loadTexture("gfx/points.png");
	directionX = 0.1;
	directionY = 1;
	width = 0;
	height = 0;
	speed = 2.5;
	reloadtime = 60;//8 frames * 60sec  = 0.13 sec
	currentReloadTime = 0;
	directionChangetime = (rand() % 300) + 180; // 3 to 8 sec
	currentDirectionChangeTime = 0;

	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
	sound = SoundManager::loadSound("sound/196914__dpoggioli__laser-gun.ogg");
	sound->volume = 64;
}
void PowerUp::update()
{
	x += directionX * speed;
	y += directionY * speed;
}
void PowerUp::draw()
{
	if (isActive == false) return;
	blitRotate(texture, x, y, 0.0f);
}
void PowerUp::setPosition(int xPos, int yPos)
{
	this->x = xPos;
	this->y = yPos;
}

int PowerUp::getPositionX()
{
	return x;
}

int PowerUp::getPositionY()
{
	return y;
}

int PowerUp::getWidth()
{
	return width;
}

int PowerUp::getHeight()
{
	return height;
}
bool PowerUp::getIsActive()
{
	return isActive;
}

void PowerUp::doActive()
{
	isActive = false;
	
}