#include "Player.h"
#include "Scene.h"
Player::~Player() 
{
	for (int i = 0; i < bullets.size(); i++) 
	{
		delete bullets[i];
	}
	bullets.clear();
}
void Player::start() 
{
	texture = loadTexture("gfx/player.png");

	x = 0;
	y = 0;
	width = 0;
	height = 0;
	speed = 4;
	reloadtime = 8;//8 frames * 60sec  = 0.13 sec
	currentReloadTime = 0;
	isAlive = true;

	SDL_QueryTexture(texture, NULL, NULL, & width, &height);
	sound = SoundManager::loadSound("sound/196914__dpoggioli__laser-gun.ogg");

}

void Player::update()
{
	for (int i = 0; i < bullets.size(); i++)
	{
		if (bullets[i]->getPositionX() > SCREEN_WIDTH)
		{
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;
			break;
		}
	}
	if (!isAlive) return;

	if (app.keyboard[SDL_SCANCODE_W])
	{
		y -= speed;
	}
	if (app.keyboard[SDL_SCANCODE_S])
	{
		y += speed;
	}	
	if (app.keyboard[SDL_SCANCODE_A])
	{
		x -= speed;
	}
	if (app.keyboard[SDL_SCANCODE_D])
	{
		x += speed;
	}
	if (currentReloadTime > 0)
		currentReloadTime--;
	if (app.keyboard[SDL_SCANCODE_F] && currentReloadTime == 0) 
	{
		SoundManager::playSound(sound);
		Bullet* bullet = new Bullet( x + width  , y - 2 + height / 2 , 1 , 0 , 10,Side::PLAYER_SIDE);
		bullets.push_back(bullet);
		getScene()->addGameObject(bullet);
		
		//reset our load timmer
		currentReloadTime = reloadtime;
	}
	
}

void Player::draw() 
{
	if (!isAlive) return;
	blit(texture, x, y);
}
int Player::getPositionX() 
{
	return x;
}
int Player::getPositionY()
{
	return y;
}

int Player::getWidth()
{
	return width;
}

int Player::getHeight()
{
	return height;
}

bool Player::getIsAlive()
{
	return isAlive;
}

void Player::doDeath()
{
	isAlive = false;
}
