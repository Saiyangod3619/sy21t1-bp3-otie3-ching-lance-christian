#include "GameScene.h"

GameScene::GameScene()
{
	// Register and add game objects on constructor
	player = new Player();
	this->addGameObject(player); 
	points = 0;
}
 
GameScene::~GameScene()
{
	delete player;
}

void GameScene::start()
{
	Scene::start();
	// Initialize any scene logic here
	initFonts();
	currentSpawnTimer = 300;
	spawnTime = 300;
	for (int i = 0;i < 5;i++) 
	{
		spawn();
		currentSpawnTimer = spawnTime;
	}
	PowerUp* powerUp = new PowerUp();
	this->addGameObject(powerUp);
	powerUp->setPosition(0 + (rand() % 300), 200);
	spawnedPowerup.push_back(powerUp);
}

void GameScene::draw()
{
	Scene::draw();
	drawText(110,20,255,255,255,TEXT_CENTER,"POINTS: %04d",points);
	if (player->getIsAlive() == false) 
	{
		drawText(SCREEN_WIDTH / 2,600,255,255,255,TEXT_CENTER,"GAME OVER :");
	}
}

void GameScene::update()
{
	Scene::update();

	doSpawnedLogic();
	doCollisionLogic();
	for (int i = 0; i < spawnedEnemies.size(); i++) 
	{
		if (spawnedEnemies[i]->getPositionY() > SCREEN_WIDTH)
		{
			Enemy* enemyDelete = spawnedEnemies[i];
			spawnedEnemies.erase(spawnedEnemies.begin() + i);
			std::cout << "enemy deleted" << std::endl;
			delete enemyDelete;
			break;
		}
		
	}
	for (int i = 0; i < spawnedPowerup.size(); i++)
	{
		if (spawnedPowerup[i]->getPositionY() > SCREEN_HEIGHT)
		{
			PowerUp* PowDelete = spawnedPowerup[i];
			spawnedPowerup.erase(spawnedPowerup.begin() + i);
			std::cout << "Power deleted" << std::endl;
			delete PowDelete;
			break;
		}
	}
	for (int i = 0; i < spawnedlevelUpEnemies.size(); i++)
	{
		if (spawnedlevelUpEnemies[i]->getPositionX() > SCREEN_WIDTH)
		{
			levelUpEnemy* BossDelete = spawnedlevelUpEnemies[i];
			spawnedlevelUpEnemies.erase(spawnedlevelUpEnemies.begin() + i);
			std::cout << "Boss deleted" << std::endl;
			delete BossDelete;
			break;
		}
	}
}
void GameScene::doSpawnedLogic()
{
	if (currentSpawnTimer > 0)
	{
		
		currentSpawnTimer--;
		if (currentSpawnTimer == 10) 
		{
			levelUpEnemy* enemyMaster = new levelUpEnemy();
			/*enemyMaster->setTexture(char "gfx/player.png");*/
			enemyMaster->setHealth(3);
			
			this->addGameObject(enemyMaster);
			enemyMaster->setPlayerTarget(player);

			enemyMaster->setPosition(0 + (rand() % 100), -10);
			spawnedlevelUpEnemies.push_back(enemyMaster);
		}
	}
	
	if (currentSpawnTimer <= 0)
	{
		Enemy* enemy = new Enemy();
		this->addGameObject(enemy);
		enemy->setPlayerTarget(player);

		enemy->setPosition(0 + (rand() % 300), -100);
		spawnedEnemies.push_back(enemy);
		currentSpawnTimer = spawnTime;
		std::cout << spawnTime << std::endl;
	}
}
void GameScene::doCollisionLogic()
{
	for (int i = 0; i < objects.size(); i++)
	{
		Bullet* bullet = dynamic_cast<Bullet*>(objects[i]);
		if (bullet != NULL)
		{
			if (bullet->getside() == Side::ENEMY_SIDE)
			{
				int collision = checkCollision(
					player->getPositionX(), player->getPositionY(), player->getWidth(), player->getHeight(),
					bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight()
				);

				if (collision == 1)
				{
					player->doDeath();
					break;
				}
			}
			else if (bullet->getside() == Side::PLAYER_SIDE)
			{
				for (int i = 0; i < spawnedEnemies.size(); i++)
				{
					Enemy* currentEnemy = spawnedEnemies[i];

					int collision = checkCollision(
						currentEnemy->getPositionX(), currentEnemy->getPositionY(), currentEnemy->getWidth(), currentEnemy->getHeight(),
						bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight()
					);					
					if (collision == 1)
					{
						
						currentEnemy->setHealth(currentEnemy->getHealth() - 1);
						
						if (currentEnemy->getHealth() == 0)
						{
							despawnEnemy(currentEnemy);
						}
						
						//5 points
						points++;
						points++;
						points++;
						points++;
						points++;
						break;
					}
				}
			}
			if (bullet->getside() == Side::ENEMY_SIDE)
			{
				int collision = checkCollision(
					player->getPositionX(), player->getPositionY(), player->getWidth(), player->getHeight(),
					bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight()
				);

				if (collision == 1)
				{
					player->doDeath();
					break;
				}
			}
			else if (bullet->getside() == Side::PLAYER_SIDE)
			{
				for (int i = 0; i < spawnedlevelUpEnemies.size(); i++)
				{
					levelUpEnemy* currentBoss = spawnedlevelUpEnemies[i];

					int collision = checkCollision(
						currentBoss->getPositionX(), currentBoss->getPositionY(), currentBoss->getWidth(), currentBoss->getHeight(),
						bullet->getPositionX(), bullet->getPositionY(), bullet->getWidth(), bullet->getHeight()
					);
					if (collision == 1)
					{
						std::cout <<"boss health " <<currentBoss->getHealth() << std::endl;
						currentBoss->setHealth(currentBoss->getHealth() - 1);
						std::cout << "boss health after "<< currentBoss->getHealth() << std::endl;
						if (currentBoss->getHealth() == 0)
						{
							despawnBoss(currentBoss);
						}

						//5 points
						points++;
						points++;
						points++;
						points++;
						points++;
						break;
					}
				}
			}
		}
	}
	for (int i = 0; i < objects.size(); i++)
	{
		PowerUp* powerUp = dynamic_cast<PowerUp*>(objects[i]);
		if (powerUp != NULL)
		{

			int collision = checkCollision(
				player->getPositionX(), player->getPositionY(), player->getWidth(), player->getHeight(),
				powerUp->getPositionX(), powerUp->getPositionY(), powerUp->getWidth(), powerUp->getHeight()
			);

			if (collision == 1)
			{
				std::cout << "power up player" << std::endl;
				player->doActive();
				delete powerUp;
				break;
			}
		}
	}

}

void GameScene::spawn()
{
	Enemy* enemy = new Enemy();
	this->addGameObject(enemy);
	enemy->setPlayerTarget(player);

	enemy->setPosition(0 + (rand() % 300), -100);
	spawnedEnemies.push_back(enemy);
}

void GameScene::despawnEnemy(Enemy* enemy)
{
	int index = -1;
	for (int i = 0; i < spawnedEnemies.size(); i++)
	{
		if (enemy == spawnedEnemies[i])
		{
			index = i;
			break;
		}
	}
	if ( index != -1)
	{
		spawnedEnemies.erase(spawnedEnemies.begin() + index);
		delete enemy;
	}
}
void GameScene::despawnBoss(levelUpEnemy* levelupenemy)
{
	int index = -1;
	for (int i = 0; i < spawnedlevelUpEnemies.size(); i++)
	{
		if (levelupenemy == spawnedlevelUpEnemies[i])
		{
			index = i;
			break;
		}
	}
	if ( index != -1)
	{
		spawnedlevelUpEnemies.erase(spawnedlevelUpEnemies.begin() + index);
		delete levelupenemy;
	}
}
