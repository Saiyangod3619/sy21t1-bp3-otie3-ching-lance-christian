#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"
#include "SoundManager.h"
#include "Bullet.h"
#include <vector>
#include "util.h"
#include "Player.h"
class PowerUp : public GameObject
{
public:
	PowerUp();
	~PowerUp();
	void start();
	void update();
	void draw();
	void setPosition(int xPos, int yPos);

	int getPositionX();
	int getPositionY();
	int getWidth();
	int getHeight();
	bool getIsActive();
	void doActive();

private:
	SDL_Texture* texture;
	Mix_Chunk* sound;
	int x;
	int y;
	float directionX;
	float directionY;

	int width;
	int height;
	int speed;
	float reloadtime;
	float currentReloadTime;
	float directionChangetime;
	float currentDirectionChangeTime;
	std::vector<PowerUp*> powerUp;
	bool isActive = true;
};

