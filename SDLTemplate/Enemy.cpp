#include "Enemy.h"
#include "Scene.h" 
#include "util.h"
Enemy::Enemy()
{

}
Enemy::~Enemy()
{
}
void Enemy::start()
{
	texture = loadTexture("gfx/enemy.png");
	directionX = -0.5;
	directionY = -1;
	width = 0;
	height = 0;
	speed = 2.5;
	health = 1;
	reloadtime = 60;//8 frames * 60sec  = 0.13 sec
	currentReloadTime = 0;
	directionChangetime = (rand() % 300) + 180; // 3 to 8 sec
	currentDirectionChangeTime = 0;

	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
	sound = SoundManager::loadSound("sound/196914__dpoggioli__laser-gun.ogg");
	sound->volume = 64;
}
void Enemy::update()
{
	x += directionX * speed;
	y += directionY * speed;
	/*texture = loadTexture(getTexture());*/

	if (currentDirectionChangeTime > 0)
		currentDirectionChangeTime--;

	if (currentDirectionChangeTime == 0) 
	{
		directionY = - directionY;
		currentDirectionChangeTime = directionChangetime;
	} 
	if (currentReloadTime > 0)
		currentReloadTime--;

	if (currentReloadTime == 0)
	{
		float dx = 0;
		float dy = -1;
		calcSlope(playerTarget->getPositionX(), playerTarget->getPositionY(), x, y, &dx, &dy);
		SoundManager::playSound(sound);
		Bullet* bullet = new Bullet(x + width , y - 2 + height / 2, dx, dy, 10,Side::ENEMY_SIDE);
		bullets.push_back(bullet);
		getScene()->addGameObject(bullet);

		//reset our load timmer
		currentReloadTime = reloadtime;
		for (int i = 0; i < bullets.size(); i++)
		{
			if (bullets[i]->getPositionX() < 0)
			{
				Bullet* bulletToErase = bullets[i];
				bullets.erase(bullets.begin() + i);
				delete bulletToErase;
				break;
			}
		}
	}
	
}
void Enemy::draw() 
{
	blitRotate(texture, x, y, -180.0f);
}
void Enemy::setPlayerTarget(Player* player)
{
	playerTarget = player;
}
void Enemy::setPosition(int xPos, int yPos) 
{
	this->x = xPos;
	this->y = yPos;
}

void Enemy::setHealth(int variableHealth)
{
	health = variableHealth;
}

//void Enemy::setTexture(const char* filename)
//{
//	texture = loadTexture(filename);
//}

int Enemy::getPositionX()
{
	return x;
}

int Enemy::getPositionY()
{
	return y;
}

int Enemy::getWidth()
{
	return width;
}

int Enemy::getHeight()
{
	return height;
}

int Enemy::getHealth()
{
	return health;
}

//char Enemy::getTexture()
//{
//	return texture;
//}
