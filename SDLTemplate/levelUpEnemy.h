
#include "GameObject.h"
#include "common.h"
#include "draw.h"
#include "SoundManager.h"
#include "Bullet.h"
#include <vector>
#include "util.h"
#include "Player.h"

class levelUpEnemy :
    public GameObject
{
public:
	levelUpEnemy();
	~levelUpEnemy();
	void start();
	void update();
	void draw();
	void setPlayerTarget(Player* player);
	void setPosition(int xPos, int yPos);
	void setHealth(int health);
	/*void setTexture(char* filename);*/


	int getPositionX();
	int getPositionY();
	int getWidth();
	int getHeight();
	int getHealth();
	/*char getTexture();*/

private:
	SDL_Texture* texture;
	Mix_Chunk* sound;
	Player* playerTarget;
	int x;
	int y;
	float directionX;
	float directionY;

	int width;
	int height;
	int speed;
	int health;
	float reloadtime;
	float currentReloadTime;
	float directionChangetime;
	float currentDirectionChangeTime;
	std::vector<Bullet*> bullets;

};
