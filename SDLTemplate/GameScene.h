#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "Player.h"
#include "Enemy.h"
#include "PowerUp.h"
#include <vector>
#include "text.h"
#include "levelUpEnemy.h"

class GameScene : public Scene
{
public:
	GameScene();
	~GameScene();
	void start();
	void draw();
	void update();
private:
	Player* player;
	Enemy* enemy;
	levelUpEnemy* levelupenemy;
	PowerUp* powerup;
	//enemy spawn
	float spawnTime;
	float currentSpawnTimer;
	std::vector<Enemy*> spawnedEnemies;
	std::vector<levelUpEnemy*> spawnedlevelUpEnemies;
	std::vector<PowerUp*> spawnedPowerup;

	void doSpawnedLogic();
	void doCollisionLogic();
	void spawn();
	
	void despawnEnemy(Enemy* enemy);
	void despawnBoss(levelUpEnemy* levelupenemy);
	
	int points;
};

