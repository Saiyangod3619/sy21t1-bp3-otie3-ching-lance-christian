#pragma once
#include "GameObject.h"
#include "Point2D.h"
#include "common.h"
#include "draw.h"
#include "SoundManager.h"
#include "util.h"
#include <vector>


class Food :
    public GameObject
{
public:
   /* Food();*/
    ~Food();
    void start();
    void update();
    void draw();
    /*coordinate foodPosition;*/



    int getFoodX();
    int getFoodY();
    int getFoodWidth();
    int getFoodHeight();
    void newFoodPosition();
   

private:
    SDL_Texture* textureFood;
    
    int foodWidth;
    int foodHeight;
    int foodX;
    int foodY;

};

