#include "Food.h"
#include "Scene.h"


//Food::Food()
//{
//}

Food::~Food()
{
}

void Food::start()
{
	textureFood = loadTexture("gfx/points.png");
	
	foodWidth = 20;
	foodHeight = 20;
	foodX = 400;
	foodY = 380;

	SDL_QueryTexture(textureFood, NULL, NULL, &foodWidth, &foodHeight);
	
}

void Food::update()
{
	
}

void Food::draw()
{
	blit(textureFood, foodX, foodY);
}

int Food::getFoodX()
{
	return foodX;
}

int Food::getFoodY()
{
	
	return foodY;
}

int Food::getFoodWidth()
{
	return foodWidth;
}

int Food::getFoodHeight()
{
	return foodHeight;
}

void Food::newFoodPosition()
{
	foodX = (rand() % 700);
	foodY = (rand() % 380);
}

