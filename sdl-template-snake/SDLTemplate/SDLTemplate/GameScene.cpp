#include "GameScene.h"
#include "GameScene.h"
GameScene::GameScene()
{
	// Register and add game objects on constructor
	snake = new Snake();
	this->addGameObject(snake);
	food = new Food();
	this->addGameObject(food);
}

GameScene::~GameScene()
{
		
}

void GameScene::start()
{
	Scene::start();
	initFonts();
}

void GameScene::draw()
{
	Scene::draw();
	drawText(110, 12, 32, 32, 255, TEXT_CENTER, "POINTS: %04d", points);
	drawText(500, 12, 32, 32, 255, TEXT_CENTER, "HIGHSCORE: %04d", highScore);
	if (snake->getIsAlive() == false)
	{
		drawText(SCREEN_WIDTH / 2, 590, 32, 32, 255, TEXT_CENTER, "GAME OVER :");
		score();
		drawText(SCREEN_WIDTH / 2,630, 32, 32, 255, TEXT_CENTER, "PRESS R TO RESTART OR E TO EXIT");
		if (app.keyboard[SDL_SCANCODE_E])
		{
			exit(4);
		}
		if (app.keyboard[SDL_SCANCODE_R])
		{
			snake->initialize();
			GameScene::start();
		}
		points = 0;
	}

}

void GameScene::update()
{
	Scene::update();
	eatFoodLogic();
}

void GameScene::eatFoodLogic()
{
	{
		for (int i = 0; i < objects.size(); i++)
		{

			if (snake != NULL)
			{
				int collision = checkCollision(snake->getSnakeX(), snake->getSnakeY(), snake->getSnakeWidth(), snake->getSnakeHeight(),
					food->getFoodX(), food->getFoodY(), food->getFoodWidth(), food->getFoodHeight());
				
				if (collision == 1)
				{
					std::cout << "Snake x :" << snake->getSnakeX() << std::endl;
					std::cout << "Snake y :" << snake->getSnakeY() << std::endl;
					std::cout << "Snake Width :" << snake->getSnakeWidth() << std::endl;
					std::cout << "Snake Height :" << snake->getSnakeHeight() << std::endl;

					std::cout << "Food x :" << food->getFoodX() << std::endl;
					std::cout << "Food y :" << food->getFoodY() << std::endl;
					std::cout << "Food Width :" << food->getFoodWidth() << std::endl;
					std::cout << "Food Height :" << food->getFoodHeight() << std::endl;

					std::cout << "Length: :" << snake->getSnakeL() << std::endl;
					
					std::cout << " prepare to grow" << std::endl; 
					snake->grow();
					food->newFoodPosition();
					points++;
				}
			}
		}
	}
	
}

void GameScene::score()
{
	
	if (points > highScore) 
	{
		
		highScore = points;
	}
}

