#include "Snake.h"
#include "Scene.h"
#include "util.h"
//#include "Grid.h"


//Snake::Snake()
//{
//}

Snake::~Snake()
{

}

void Snake::start()
{
	// load texture
	texture = loadTexture("gfx/playerBullet.png");
	width = 1;
	height = 1;
	length = 5;
	speed = 1;
	direction.x = 3;
	direction.y = 0;
	gridSize = 3;
	
	isAlive = true;
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
	sound = SoundManager::loadSound("sound/F5YUGD6-game-award.mp3");
}

void Snake:: update()
{
	if (app.keyboard[SDL_SCANCODE_UP])
	{
		up();
	}

	if (app.keyboard[SDL_SCANCODE_DOWN])
	{
		down();
	}
	if (app.keyboard[SDL_SCANCODE_LEFT])
	{
		left();
	}
	if (app.keyboard[SDL_SCANCODE_RIGHT])
	{
		right();
	}
	if (app.keyboard[SDL_SCANCODE_A])
	{
		grow();
	}
}

void Snake:: draw()
{
	if (!isAlive) return;
	crawl();
	Death= SoundManager::loadSound("sound/mixkit-player-losing-or-failing-2042.wav");
	if (collisionWall()) {
		std::cout << "wall collision" << std::endl;
		std::cout << SCREEN_WIDTH << " HEIGHT : " << SCREEN_HEIGHT << std::endl;
		isAlive = false;
		SoundManager::playSound(Death);
	}
	if (collisionSelf())
	{
		isAlive = false;
		SoundManager::playSound(Death);
	}
	
	
	for (int i = 0; i < length; i++)
	{	
		blit(texture, Snakeposition[i].x,Snakeposition[i].y);
	}
}

void Snake:: up()
{
	if ( direction.y == 0)
	{
		 direction.x = 0;
		 direction.y = -(gridSize);
	}
}

void Snake::down()
{
	if (direction.y == 0)
	{
		direction.x = 0;
		direction.y = gridSize;
	}
}

void Snake::right()
{
	if (direction.x == 0)
	{
		direction.x =gridSize;
		direction.y = 0;
	}
}

void Snake::left()
{
	if (direction.x == 0)
	{
		direction.x = -(gridSize);
		direction.y = 0;
	}
}

void Snake::crawl()
{
	for (int i = length - 1; i > 0; i--)
	{
		Snakeposition[i].x = Snakeposition[i - 1].x;
		Snakeposition[i].y = Snakeposition[i - 1].y;
	}
	Snakeposition[0].x = Snakeposition[0].x + direction.x;
	Snakeposition[0].y = Snakeposition[0].y + direction.y;
}

bool Snake::collisionWall()
{
	if (Snakeposition[0].x < 0) return true;
	if (Snakeposition[0].x > SCREEN_WIDTH) return true;
	if (Snakeposition[0].y < 0) return true;
	if (Snakeposition[0].y > SCREEN_HEIGHT) return true;
	return false;
}

bool Snake::collisionSelf() 
{
	
	for (int i = 5; i < length; i++)
	{
		int collision = checkCollision(Snakeposition[0].x, Snakeposition[0].y, 5,5,
			Snakeposition[i].x, Snakeposition[i].y, 5, 5);

		if (collision == 1)
		{
			std::cout << "x head =" << Snakeposition[0].x <<
				"y head =" << Snakeposition[0].y <<
				" head width =" << getSnakeWidth() <<
				" head height= " << getSnakeHeight() <<
				" body x =" << Snakeposition[i].x <<
				" body y = " << Snakeposition[i].y <<
				" body width = " << getSnakeWidth() <<
				"body height = " << getSnakeHeight();
			std::cout << "Self Collision warning" << std::endl;
			return true;
		}
		
	}
	return false;
}


void Snake::grow()
{
	Snakeposition[length].x = Snakeposition[length - 1].x;
	Snakeposition[length].y = Snakeposition[length - 1].y;
	length += (gridSize *2);
	SoundManager::playSound(sound);
}

void Snake::initialize()
{
	width = 1;
	height = 1;
	length = 5;
	speed = 1;
	direction.x = 3;
	direction.y = 0;
	gridSize = 3;

	isAlive = true;
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
	Snakeposition[0].x = 200;
	Snakeposition[0].y = 200;


}

int Snake::getSnakeX()
{
	return Snakeposition[0].x;
}

int Snake::getSnakeY()
{
	return Snakeposition[0].y;
}

int Snake::getSnakeWidth()
{
	return width;
}

int Snake::getSnakeHeight()
{
	return height;
}
bool Snake::getIsAlive()
{
	return isAlive;
}

int Snake::getSnakeL()
{
	return length;
}
