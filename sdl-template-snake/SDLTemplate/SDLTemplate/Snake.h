#pragma
#include "GameObject.h"
#include "common.h"
#include "draw.h"
#include "SoundManager.h"
#include "Point2D.h"
#include "util.h"
#include <vector>

class Snake :
    public GameObject
{
public:
    /*Snake();*/
    ~Snake();
    void start();
    void update();
    void draw();
    coordinate Snakeposition[1000];
    coordinate direction;

    
    void up();
    void down();
    void right();
    void left();
    void crawl();
    bool collisionWall();
    bool collisionSelf();
    void grow();
    void initialize();
    int getSnakeX();
    int getSnakeY();
    int getSnakeWidth();
    int getSnakeHeight();
    bool getIsAlive();
    int getSnakeL();
private:
    SDL_Texture* texture;
    Mix_Chunk* sound;
    Mix_Chunk* Death;
    
    int width;
    int height;
    int speed;
    int length;
    bool isAlive;
    int gridSize;
};

