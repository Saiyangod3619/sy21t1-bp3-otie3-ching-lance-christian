#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "Snake.h"
#include "Food.h"
#include <vector>
#include "text.h"
#include "util.h"
#include "SoundManager.h"
#include <fstream>
class GameScene : public Scene
{
public:
	GameScene();
	~GameScene();
	void start();
	void draw();
	void update();
	FILE* f;
private:
	Mix_Chunk* sound;
	Snake* snake;
	Food* food;
	void eatFoodLogic();
	int points;
	int highScore;
	void score();
};

